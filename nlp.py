from sklearn.metrics.pairwise import cosine_similarity
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import numpy as np

df = pd.read_csv('./qa.csv')
X = df.Pertanyaan
answer = df.Jawaban
stop_words = ['di', 'saya', 'untuk', '?']

text_clf = Pipeline([
    ('vect', CountVectorizer(stop_words=stop_words)),
    ('tfidf', TfidfTransformer())
])
X_transform = text_clf.fit_transform(X)

def get_answer(question):
    y_transform = text_clf.transform([question])
    similarity = cosine_similarity(X_transform,y_transform)
    highest_value = max(similarity)[0]
    highest_index = np.where(similarity == highest_value)[0][0]
    result = '<b>'+X[highest_index] + '</b></br></br>' + answer[highest_index] if highest_value > 0.5 else 'Maaf saya tidak bisa memahaminya'
    result = result.replace('\n', '</br>')
    return result