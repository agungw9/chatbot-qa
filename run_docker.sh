TIMEOUT=${1:-60}

NAME="chatbot" && \
docker build --rm -t $NAME . && \
docker rm -f $NAME && \
docker run -d -p 5000:5000 --name $NAME $NAME $TIMEOUT
