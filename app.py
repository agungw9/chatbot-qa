from chatterbot import ChatBot
from flask import Flask, render_template, request
from nlp import get_answer
import sys

TIMEOUT = 30

app = Flask(__name__)
app.static_folder = 'static'

@app.route("/")
def home():
    return render_template("index.html", timeout=TIMEOUT)

@app.route("/get")
def get_bot_response():
    userText = str(request.args.get('msg'))
    if userText == 'timeout':
        return "Terima kasih telah menggunakan layanan ini"
    
    answer = get_answer(userText)
    return answer

if __name__ == "__main__":
    TIMEOUT = int(sys.argv[1]) if len(sys.argv) > 1 else TIMEOUT
    app.run(host='0.0.0.0', port=5000)