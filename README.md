# Chatbot QA
Conversation agent or Chatbot that returns an answer based on a set of question and answer (QA) pairs.

[![Github](http://i.imgur.com/9I6NRUm.png) Github](https://github.com/aw09)<br>
[![Linkedin](https://i.stack.imgur.com/gVE0j.png) LinkedIn](https://www.linkedin.com/in/agungw9/) <br>
[Demo](http://34.101.133.218:5000/)

# Table of Content
- [Installation](#installation)
  - [Using Virtual Env](#using-virtual-env)
  - [Using Docker](#using-docker)
- [Change Timeout](#change-timeout)


# Installation

Clone the repository
```bash
    git clone https://gitlab.com/agungw9/chatbot-qa.git
```


## Using Virtual Env

Tools needed :
 * Python >= 3.8
 * Virtualenv

Make and activate virtualenv
```bash
  virtualenv .
  ./Scripts/activate # Windows
  source bin/activate # Linux
```
Install the required libraries
```
    pip install -r requirements.txt
```
Run program
```
    flask run
```
Check on
```
    localhost:5000
```
## Using Docker
Tools needed :
 * Docker

I have created Dockerfile and created bash file to build docker image and run it, just make that file executable using
```bash
    chmod +x run_docker.sh
```
Then run that file
```
    ./run_docker.sh
```
Check running container
```
    docker ps
```
Check on
```
    ipserver:5000
```
# Change timeout
Add seconds parameter
Example for 60 seconds
```bash
    ./run_docker.sh 60
```